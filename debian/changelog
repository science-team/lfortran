lfortran (0.45.0-1) unstable; urgency=medium

  * New upstream release
  * Build-dep on re2c,bison for build0.sh script
  * Build-dep on libunwind-dev
  * Call build0.sh before configure
  * Add LSP support
  * Patch for cmake  breaking autopkgtest. Closes: #1094444

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 01 Feb 2025 19:54:57 +0000

lfortran (0.42.0-1) unstable; urgency=medium

  * New upstream release
  * Move to llvm-19. Closes: #1081244

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 25 Nov 2024 19:42:19 +0000

lfortran (0.40.0-1) unstable; urgency=medium

  * New upstream release

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 04 Nov 2024 05:02:25 +0000

lfortran (0.36.0-1) unstable; urgency=medium

  * New upstream release
  * Set Debian Science Maint. as maintainer
  * Standards-Version: 4.7.0; no changes required
  * Include full BSD license as lintian hints
  * Don't depend on python:minimal

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 04 Jun 2024 17:31:56 +0100

lfortran (0.35.0-1) unstable; urgency=medium

  * New upstream release
  * Don't use DWZ; fails on s390x
  * Build with DWARFDUMP=yes, KOKKOS=yes
  * Ship /usr/share/lfortran/*
  * Depend on python3 for scripts

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 16 Apr 2024 18:44:14 +0100

lfortran (0.30.0-5) unstable; urgency=medium

  * autopkgtests need to dep on build-essential. Closes: #1062813, #1063056

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 11 Feb 2024 17:55:26 +0000

lfortran (0.30.0-4) unstable; urgency=medium

  * Disable KOKKOS and cpp-backend test. Currently break the build.
    Closes: #1062813

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 03 Feb 2024 16:13:58 +0000

lfortran (0.30.0-3) unstable; urgency=medium

  * Patch to ensure we use shared zstd lib, not static
  * Build-dep on pandoc for man page
  * Add lfortran.1 manpage
  * Add WITH_KOKKOS=yes for cpp backend
  * clang-16 needed for specified llvm backend
  * Add autopkgtests from  example files
  * Build-dep on xeus-dev, xeus-zmq-dev for Jupyter kernel support
  * Don't use shared bfd library. Closes: #1061935
  * Add Built-Using for BFD  (+ iberty, sframe)
  * Fix VCS  repo to science-team and push to salsa

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 30 Jan 2024 14:22:10 +0000

lfortran (0.30.0-2) unstable; urgency=medium

  * Push to unstable

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 24 Jan 2024 19:48:26 +0000

lfortran (0.30.0-1) experimental; urgency=medium

  * Initial release. (Closes: #1057266)
  * Patch to ensure we don't include statically linked zlib

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 23 Jan 2024 16:10:25 +0000
